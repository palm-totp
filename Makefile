SDK		?= /opt/palm/sdk-5r3/include
PILRC		= pilrc
CC		= m68k-palm-elf-gcc
LD		= m68k-palm-elf-gcc
OBJCOPY		= m68k-palm-elf-objcopy
COMMON		= -Wno-multichar -Os -m68000 -mno-align-int -mpcrel -fpic -fshort-enums -mshort
WARN		= -Wsign-compare -Wextra -Wall -Werror -Wno-unused-parameter -Wno-old-style-declaration -Wno-unused-function -Wno-unused-variable -Wno-error=cpp -Wno-error=switch
LKR		= linker.lkr
CCFLAGS		= $(LTO) $(WARN) $(COMMON) -I. -ffunction-sections -fdata-sections
LDFLAGS		= $(LTO) $(WARN) $(COMMON) -Wl,--gc-sections -Wl,-T $(LKR)

SRCS		= src/main.c src/mainform.c src/codetable.c src/dialogs.c src/record.c src/sha1.c src/otp.c src/base32.c src/util.c
RCP		= src/totp.rcp
RCP_COMMON	= src/version.rcp
RSC		= src/
OBJS		= $(patsubst %.S,%.o,$(patsubst %.c,%.o,$(SRCS)))
TARGET		= TOTPAuth
CREATOR		= TOTP
TYPE		= appl

INCS		+= -isystem$(SDK)
INCS		+= -isystem$(SDK)/Core
INCS		+= -isystem$(SDK)/Core/Hardware
INCS		+= -isystem$(SDK)/Core/System
INCS		+= -isystem$(SDK)/Core/UI
INCS		+= -isystem$(SDK)/Dynamic
INCS		+= -isystem$(SDK)/Libraries
INCS		+= -isystem$(SDK)/Libraries/PalmOSGlue

.PHONY: all
all: $(TARGET).prc

%.prc:
	$(PILRC) -ro -o $@ -creator $(CREATOR) -type $(TYPE) -name "TOTP Authenticator" -I $(RSC) $<
$(TARGET).prc: APPNAME = Authenticator
$(TARGET).prc: $(RCP) $(RCP_COMMON) code.bin

%.bin: %.elf
	$(OBJCOPY) -O binary $< $@ -j.vec -j.text -j.rodata

code.elf: $(OBJS)
	$(LD) -o $@ $(LDFLAGS) $^

%.o : %.c Makefile
	$(CC) $(CCFLAGS)  $(INCS) -c $< -o $@

EMU_IMAGE = palm3-image

.PHONY: emu
emu: $(TARGET).prc
	echo "launch Preferences" > /tmp/cloudpilot-script
	echo "install $(TARGET).prc" >> /tmp/cloudpilot-script
	echo "launch \"TOTP Authenticator\"" >> /tmp/cloudpilot-script
	cloudpilot-emu -s /tmp/cloudpilot-script $(EMU_IMAGE)

.PHONY: debug
debug: $(TARGET).prc
	echo "launch Preferences" > /tmp/cloudpilot-script
	echo "install $(TARGET).prc" >> /tmp/cloudpilot-script
	echo "launch $(TARGET)" >> /tmp/cloudpilot-script
	cloudpilot-emu -l 2345 --wait-for-attach --debug-app code.elf \
	       	-s /tmp/cloudpilot-script $(EMU_IMAGE)

.PHONY: lint
lint: code.elf
	grep -r TODO src || true
	! grep -r XXX src
	! (nm -j code.elf | grep -q printf && echo "warning: printf in palm executable")

.PHONY: clean
clean:
	rm -f $(OBJS) code.elf code.bin $(TARGET).prc
