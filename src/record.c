#include <PalmOS.h>
#include "app.h"

void UnpackKeyRecord(struct key_record *rec, void *p)
{
    MemMove(&rec->header, p, sizeof(struct key_record_header));
    p += sizeof(struct key_record_header);
    rec->key = p;
    p += rec->header.key_len;
    rec->appname = p;
    p += StrLen(p) + 1;
    rec->username = p;
    p += StrLen(p) + 1;
}

UInt32 KeyRecordSize(const struct key_record *rec)
{
    return sizeof(struct key_record_header) + rec->header.key_len
        + StrLen(rec->appname) + StrLen(rec->username) + 2;
}

void PackKeyRecord(void *p, const struct key_record *rec)
{
    UInt32 offset = 0;
    DmWrite(p, offset, &rec->header, sizeof(struct key_record_header));
    offset += sizeof(struct key_record_header);
    if(rec->key) DmWrite(p, offset, rec->key, rec->header.key_len);
    offset += rec->header.key_len;
    DmWrite(p, offset, rec->appname, StrLen(rec->appname)+1);
    offset += StrLen(rec->appname)+1;
    DmWrite(p, offset, rec->username, StrLen(rec->username)+1);
    offset += StrLen(rec->username)+1;
}
