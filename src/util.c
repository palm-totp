#include <PalmOS.h>
#include "app.h"

Int16 DefaultTimezoneV2()
{
    Int16 tz = PrefGetPreference(prefMinutesWestOfGMT)/60;
    if(tz > 12) tz -= 24;
    return tz;
}

Int16 DefaultTimezoneV4()
{
    UInt32 raw_tz = PrefGetPreference(prefTimeZone);
    Int16 tz = *((Int32 *)&raw_tz) / 60;
    Int16 dst = PrefGetPreference(prefDaylightSavingAdjustment) / 60;
    return tz + dst;
}

UInt16 GetRomVerMajor()
{
    UInt32 dwROMVer;
    FtrGet(sysFtrCreator, sysFtrNumROMVersion, &dwROMVer);
    return sysGetROMVerMajor(dwROMVer);
}

void GetPrefs(struct myPrefs *p)
{
    if(!PrefGetAppPreferencesV10(APP_CREATOR, PREFS_VERSION, (void *)p, sizeof(struct myPrefs))){
        p->autoTZ = true;
        p->timezone = 0;
        if(GetRomVerMajor() >= 4){
            p->timezone = DefaultTimezoneV4();
        } else if(GetRomVerMajor() >= 2){
            p->timezone = DefaultTimezoneV2();
        }
        p->offsetSeconds = 0;
    }
}

UInt32 UnixNow()
{
    DateTimeType epoch;
    epoch.second = 0;
    epoch.minute = 0;
    epoch.hour = 0;
    epoch.day = 1;
    epoch.month = 1;
    epoch.year = 1970;
    struct myPrefs prefs;
    GetPrefs(&prefs);
    Int32 offset = prefs.timezone * 3600;
    if(prefs.autoTZ && GetRomVerMajor() >= 4){
        offset = DefaultTimezoneV4() * 3600;
    }
    return TimGetSeconds() - TimDateTimeToSeconds(&epoch) - offset;
}

/* Zero-pad the OTP code, because Palm's sprintf is missing that feature */
void ZeroPad(char *out, UInt32 code, int desired_len)
{
    StrPrintF(out, "%lu", code);
    int actual_len = StrLen(out);
    if(actual_len == desired_len) return;
    for(int i = 1; i <= actual_len; ++i){
        out[desired_len - i] = out[actual_len - i];
    }
    out[desired_len] = 0;
    for(int i = 0; i < desired_len - actual_len; ++i){
        out[i] = '0';
    }
}

void Hex(char *out, const UInt8 *in, UInt16 in_len)
{
    for(UInt16 i = 0; i < in_len; ++i){
        UInt8 n = in[i] >> 4;
        if(n>9) *(out++) = 87+n;
        else *(out++) = 48+n;
        n = in[i] & 0x0f;
        if(n>9) *(out++) = 87+n;
        else *(out++) = 48+n;
    }
    *out = 0;
}
