#ifndef APP_H
#define APP_H
#include <PalmOS.h>
#include "otp.h"

#define APP_CREATOR 'TOTP'
#define PREFS_VERSION 1

#define FTR_DB_HANDLE 0
#define FTR_TICK_ENABLE 1
#define FTR_TABLE_SEL 2
#define FTR_LAST_TICK 3

struct myPrefs {
    UInt8 unused1:7;
    UInt8 autoTZ:1;
    Int8 timezone;
    UInt16 unused2;
    Int32 offsetSeconds;
};

enum {
    OTP_TYPE_TOTP,
    OTP_TYPE_HOTP,
    OTP_TYPE_STEAM
};

#pragma pack(push,1)
struct key_record_header {
    UInt8 type:4;
    UInt8 code_len:4;
    UInt8 key_len;
    UInt16 unused;
    UInt32 counter;
    // followed by key, appname, username
};
#pragma pack(pop)

struct key_record {
    struct key_record_header header;
    UInt8 *key;
    char *appname;
    char *username;
};

/* record.c */
void UnpackKeyRecord(struct key_record *rec, void *p);
uint32_t KeyRecordSize(const struct key_record *rec);
void PackKeyRecord(void *p, const struct key_record *rec);

/* codetable.c */
void CodeTableInit(FormType *form);
void CodeTableScroll(FormType *form, int d);
void CodeTableRefresh(FormType *form);
Boolean CodeTablePenDown(FormType *form, EventType *event);
Int16 CodeTableGetSelection();
void CodeTableSelect(TableType *tbl, Int16 row);
void CodeTableShowHOTP(TableType *tbl, Int16 row);

/* dialogs.c */
Boolean DoNewKeyDialog();
Boolean DoDetailsDialog(UInt16 recIdx);
void DoTimezoneDialog();

/* util.c */
UInt32 UnixNow();
Int16 DefaultTimezoneV2();
Int16 DefaultTimezoneV4();
UInt16 GetRomVerMajor();
void GetPrefs(struct myPrefs *prefs);
void ZeroPad(char *out, UInt32 code, int desired_len);
void Hex(char *out, const UInt8 *in, UInt16 in_len);

#endif
