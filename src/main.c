#include <PalmOS.h>
#include "res.h"
#include "app.h"

Boolean MainFormHandleEvent(EventType *event);
Boolean TimezoneHandleEvent(EventType *event);

static Boolean AppHandleEvent(EventType *event){
    FormType *form;

    switch(event->eType){
    case frmLoadEvent:
        form = FrmInitForm(event->data.frmLoad.formID);
        FrmSetActiveForm(form);
        switch(event->data.frmLoad.formID){
        case MainForm:
            FrmSetEventHandler(form, MainFormHandleEvent);
            break;
        case TimezoneDialog:
            FrmSetEventHandler(form, TimezoneHandleEvent);
            break;
        }
        return true;
    default:
        return false;
    }
}

static void AppEventLoop(void)
{
    Err error;
    EventType event;
    do {
        //EvtGetEvent(&event, evtWaitForever);
	EvtGetEvent(&event, sysTicksPerSecond); //TODO turn off clock tick when unneeded
        if(!SysHandleEvent(&event))
            if(!MenuHandleEvent(NULL, &event, &error))
                if(!AppHandleEvent(&event))
                    FrmDispatchEvent(&event);
    } while (event.eType != appStopEvent);
}

static void AppStart(void){
    LocalID dbId = DmFindDatabase(0, "TOTP Keys");
    if(!dbId){
        DmCreateDatabase(0, "TOTP Keys", APP_CREATOR, 'data', false);
        dbId = DmFindDatabase(0, "TOTP Keys");
    }
    DmOpenRef db = DmOpenDatabase(0, dbId, dmModeReadWrite);
    FtrSet(APP_CREATOR, FTR_DB_HANDLE, (UInt32)db);

    FtrSet(APP_CREATOR, FTR_TICK_ENABLE, 0);
    FtrSet(APP_CREATOR, FTR_LAST_TICK, 0);
    FtrSet(APP_CREATOR, FTR_TABLE_SEL, 0xffff);
}

static void AppStop(void){
    DmOpenRef db;
    FtrGet(APP_CREATOR, FTR_DB_HANDLE, (UInt32 *)&db);
    DmCloseDatabase(db);
    FrmCloseAllForms();
}

UInt32 PilotMain(UInt16 cmd, void *cmdPBP, UInt16 launchFlags) {
    if (sysAppLaunchCmdNormalLaunch == cmd) {
        AppStart();
        FrmGotoForm(MainForm);
        AppEventLoop();
        AppStop();
    }

    return 0;
}

UInt32 __attribute__((section(".vectors"))) __Startup__(void)
{
    SysAppInfoPtr appInfoP;
    void *prevGlobalsP;
    void *globalsP;
    UInt32 ret;
    
    SysAppStartup(&appInfoP, &prevGlobalsP, &globalsP);
    ret = PilotMain(appInfoP->cmd, appInfoP->cmdPBP, appInfoP->launchFlags);
    SysAppExit(appInfoP, prevGlobalsP, globalsP);
    
    return ret;
}
