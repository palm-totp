#include <stddef.h>
#include <PalmOS.h>
#include "res.h"
#include "app.h"
#include "sha1.h"
#include "otp.h"
#include "base32.h"

Int16 CodeTableGetSelection()
{
    UInt32 sel;
    FtrGet(APP_CREATOR, FTR_TABLE_SEL, &sel);
    return sel != 0xffffffff ? (Int16)sel : -1;
}

static void _DrawCode(char *codestr, RectangleType *bounds, Boolean inverted)
{
#define _PUTS(_S, _L, _X, _Y) inverted ? \
	WinDrawInvertedChars(_S, _L, bounds->topLeft.x+(_X), bounds->topLeft.y+(_Y)) : \
	WinDrawChars(_S, _L, bounds->topLeft.x+(_X), bounds->topLeft.y+(_Y))
    FontID oldFont = FntSetFont(ledFont);
    _PUTS(codestr, 3, 107-3, 3);
    _PUTS(codestr+3, 3, 134, 3);
    FntSetFont(oldFont);
#undef _PUTS
}

void DrawCodeRow(void *table, Int16 row, Int16 col, RectangleType *bounds)
{
#define _PUTS(_S, _L, _X, _Y) WinDrawChars(_S, _L, bounds->topLeft.x+(_X), bounds->topLeft.y+(_Y))
    DmOpenRef db;
    FtrGet(APP_CREATOR, FTR_DB_HANDLE, (UInt32 *)&db);
    MemHandle recH = DmQueryRecord(db, TblGetItemInt(table, row, col));
    struct key_record rec;
    UnpackKeyRecord(&rec, MemHandleLock(recH));
    MemHandle ledDot = DmGetResource('Tbmp', LedDot);
    BitmapType *bitmap;

    FontID oldFont = FntSetFont(boldFont);
    _PUTS(rec.appname, StrLen(rec.appname), 0, 1);
    FntSetFont(stdFont);
    _PUTS(rec.username, StrLen(rec.username), 0, 12);

    RectangleType r;
    r.topLeft.x = bounds->topLeft.x+100;
    r.topLeft.y = bounds->topLeft.y;
    r.extent.x = bounds->extent.x-100;
    r.extent.y = bounds->extent.y;
    WinEraseRectangle(&r, 0);

    struct otp_options options;
    options.code_length = rec.header.code_len;
    options.code_base = 10;
    options.timestep = 30;
    options.t0 = rec.header.counter;
    UInt32 code;
    char codebuf[9];
    switch(rec.header.type){
    case OTP_TYPE_HOTP:
        //code = hotp(rec->key, rec->key_len, rec->counter, &options);
        bitmap = MemHandleLock(ledDot);
        for(Coord x = 107-3; x < 160; x += 9){
            WinDrawBitmap(bitmap, bounds->topLeft.x+x, bounds->topLeft.y+3);
            if(x == 122) x += 3;
        }
        MemHandleUnlock(ledDot);
        break;
    case OTP_TYPE_TOTP:
        //options.t0 = rec.header.counter;
        code = totp(rec.key, rec.header.key_len, UnixNow(), &options);
        ZeroPad(codebuf, code, options.code_length);
        _DrawCode(codebuf, bounds, false);
        break;
    default: return;
    }

    if(CodeTableGetSelection() == row){
        WinInvertRectangle(bounds, 0);
    }

    FntSetFont(oldFont);
    MemHandleUnlock(recH);
#undef _PUTS
}

void CodeTableScroll(FormType *form, int d)
{
    TableType *tbl = FrmGetObjectPtr(form, FrmGetObjectIndex(form, CodeTable));
    Int16 base = TblGetItemInt(tbl, 0, 0) + d;
    DmOpenRef db; FtrGet(APP_CREATOR, FTR_DB_HANDLE, (UInt32 *)&db);
    Int16 max = DmNumRecordsInCategory(db, dmAllCategories);
    while(base >= max){
        if(base <= 0) break;
        base -= 5;
    }
    if(base < 0) base = 0;
    Int16 nrows = TblGetNumberOfRows(tbl);
    for(int row = 0; row < nrows; ++row){
        TblSetItemInt(tbl, row, 0, base+row);
        TblSetRowUsable(tbl, row, base+row < max);
    }
    TblMarkTableInvalid(tbl);
    CodeTableSelect(tbl, -1);
    TblRedrawTable(tbl);
    ControlType *ctl;
    ctl = FrmGetObjectPtr(form, FrmGetObjectIndex(form, UpBtn));
    if(base > 0){
        CtlSetLabel(ctl, "\x01");
        CtlSetEnabled(ctl, true);
    } else {
        CtlSetLabel(ctl, "\x03");
        CtlSetEnabled(ctl, false);
    }
    ctl = FrmGetObjectPtr(form, FrmGetObjectIndex(form, DownBtn));
    if(base + 5 < max){
        CtlSetLabel(ctl, "\x02");
        CtlSetEnabled(ctl, true);
    } else {
        CtlSetLabel(ctl, "\x04");
        CtlSetEnabled(ctl, false);
    }
}

void CodeTableRefresh(FormType *form)
{
    TableType *tbl = FrmGetObjectPtr(form, FrmGetObjectIndex(form, CodeTable));
    DmOpenRef db;
    FtrGet(APP_CREATOR, FTR_DB_HANDLE, (UInt32 *)&db);
    Int16 nrows = TblGetNumberOfRows(tbl);
    MemHandle recH;
    struct key_record rec;
    for(int row = 0; row < nrows; ++row){
        if(!TblRowUsable(tbl, row)) continue;
        recH = DmQueryRecord(db, TblGetItemInt(tbl, row, 0));
        UnpackKeyRecord(&rec, MemHandleLock(recH));
        if(rec.header.type == OTP_TYPE_TOTP){
            TblMarkRowInvalid(tbl, row);
        }
        MemHandleUnlock(recH);
    }
    TblRedrawTable(tbl);
}

void CodeTableShowHOTP(TableType *tbl, Int16 row)
{
    DmOpenRef db;
    FtrGet(APP_CREATOR, FTR_DB_HANDLE, (UInt32 *)&db);
    UInt16 recIdx = TblGetItemInt(tbl, row, 0);
    MemHandle recH = DmGetRecord(db, recIdx);
    void *recP = MemHandleLock(recH);
    struct key_record rec;
    UnpackKeyRecord(&rec, recP);
    ++rec.header.counter;

    struct otp_options options;
    options.code_length = rec.header.code_len;
    options.code_base = 10;
    UInt32 code = hotp(rec.key, rec.header.key_len, rec.header.counter, &options);
    char codebuf[9];
    ZeroPad(codebuf, code, options.code_length);
    RectangleType bounds;
    TblGetItemBounds(tbl, row, 0, &bounds);
    _DrawCode(codebuf, &bounds, true);

    DmWrite(recP, offsetof(struct key_record_header, counter), &rec.header.counter, sizeof(UInt32));
    MemHandleUnlock(recH);
    DmReleaseRecord(db, recIdx, true);
}

void CodeTableInit(FormType *form)
{
    TableType *tbl = FrmGetObjectPtr(form, FrmGetObjectIndex(form, CodeTable));
    FtrSet(APP_CREATOR, FTR_TABLE_SEL, 0xffffffff);
    TblSetColumnUsable(tbl, 0, true);
    TblSetCustomDrawProcedure(tbl, 0, DrawCodeRow);
    Int16 nrows = TblGetNumberOfRows(tbl);
    for(int row = 0; row < nrows; ++row){
        TblSetRowUsable(tbl, row, false);
        TblSetItemStyle(tbl, row, 0, customTableItem);
        TblSetItemInt(tbl, row, 0, row);
        TblSetItemPtr(tbl, row, 0, NULL);
        TblSetRowSelectable(tbl, row, false); // replaced with custom selection code
        TblSetRowHeight(tbl, row, 25);
    }
    CodeTableScroll(form, 0);
}

void CodeTableSelect(TableType *tbl, Int16 row)
{
    RectangleType r;
    Int16 nrows = TblGetNumberOfRows(tbl);
    Int16 oldSelection = CodeTableGetSelection();
    UInt32 selection = 0xffffffff;
    if(row == oldSelection) return;
    TblGetBounds(tbl, &r);
    for(Int16 i = 0; i < nrows; ++i){
        r.extent.y = TblGetRowHeight(tbl, i);
        if(i == oldSelection){
            WinInvertRectangle(&r, 0);
        }
        if(i == row){
            WinInvertRectangle(&r, 0);
            selection = row;
        }
        r.topLeft.y += r.extent.y;
    }
    FtrSet(APP_CREATOR, FTR_TABLE_SEL, selection);
}

Boolean CodeTablePenDown(FormType *form, EventType *event)
{
    TableType *tbl = FrmGetObjectPtr(form, FrmGetObjectIndex(form, CodeTable));
    Int16 nrows = TblGetNumberOfRows(tbl);
    Coord x = event->screenX;
    Coord y = event->screenY;
    RectangleType bounds;
    TblGetBounds(tbl, &bounds);
    if(!RctPtInRectangle(x, y, &bounds)) return false;
    for(Int16 i = 0; i < nrows; ++i){
        bounds.extent.y = TblGetRowHeight(tbl, i);
        if(TblRowUsable(tbl, i) && RctPtInRectangle(x, y, &bounds)){
            CodeTableSelect(tbl, i);
            break;
        }
        bounds.topLeft.y += bounds.extent.y;
    }
    return true;
}
