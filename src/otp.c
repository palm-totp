#include "otp.h"

#include <stdint.h>
#include "sha1.h"

uint32_t hotp(const uint8_t *key, uint16_t keylen, uint32_t counter, const struct otp_options *options)
{
    uint8_t digest[20];
    uint32_t c[2];
    c[0] = 0;
    c[1] = counter;
    hmac_sha1((uint32_t *)digest, key, (uint8_t *)c, keylen, 8);
    uint8_t offset = digest[19] & 0xf;
    uint32_t code = ((uint32_t)(digest[offset] & 0x7f) << 24)
        | ((uint32_t)digest[offset+1] << 16)
        | ((uint32_t)digest[offset+2] << 8)
        | digest[offset+3];
    uint32_t divisor = options->code_base;
    for(int i = 0; i < options->code_length - 1; ++i)
        divisor *= options->code_base;
    code %= divisor;
    return code;
}

uint32_t totp(const uint8_t *key, uint16_t keylen, uint32_t when, const struct otp_options *options)
{
    return hotp(key, keylen, (when - options->t0) / options->timestep, options);
}

void totp_steam_format(char *out, uint32_t code)
{
    const char *alphabet = "23456789BCDFGHJKMNPQRTVWXY";
    for(int i = 0; i < 5; ++i){
        *(out++) = alphabet[code % 26];
        code /= 26;
    }
    *out = 0;
}
