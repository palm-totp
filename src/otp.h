#ifndef OTP_H
#define OTP_H

#include <stdint.h>

struct otp_options {
    uint16_t code_length:4; /* Number of digits in the generated password */
    uint16_t code_base:6;   /* Base of each digit (base-10, base-26, etc) */
    uint16_t unused:6;
    uint16_t timestep;      /* Seconds between new TOTP values */
    uint32_t t0;            /* Epoch for TOTP calculation */
};

uint32_t hotp(const uint8_t *key, uint16_t keylen, uint32_t counter, const struct otp_options *options);
uint32_t totp(const uint8_t *key, uint16_t keylen, uint32_t when, const struct otp_options *options);

#endif
