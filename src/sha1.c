#include <string.h>
#include "sha1.h"

void sha1(uint32_t *digest, const uint8_t *data, uint16_t len)
{
    struct sha1_context ctx;
    sha1_begin(digest, &ctx, data, len);
    while(sha1_continue(digest, &ctx, true));
}

void sha1_begin(uint32_t *digest, struct sha1_context *ctx, const uint8_t *data, uint16_t len)
{
    digest[0] = 0x67452301;
    digest[1] = 0xefcdab89;
    digest[2] = 0x98badcfe;
    digest[3] = 0x10325476;
    digest[4] = 0xc3d2e1f0;
    ctx->data = data;
    ctx->total_len = len;
    ctx->remaining = len;
}

bool sha1_continue(uint32_t *digest, struct sha1_context *ctx, bool finish)
{
    uint32_t w[80];
    uint8_t *w_bytes = (uint8_t *)w;
    uint8_t len = 64;
    bool more = true;

    if(ctx->remaining < 64){
        if(!finish) return false;
        len = ctx->remaining;
    }

    // copy chunk
    memcpy(w_bytes, ctx->data, len);
    ctx->data += len;
    ctx->remaining -= len;

    if(len < 64){
        // pad data to 16 words (64 bytes)
	memset(w_bytes+len, 0, 64-len);
        // append 1 bit
        w_bytes[len] = 0x80;
        if(len < 56){
            // append message length in bits
            w[15] = ctx->total_len << 3;
            more = false;
        }
    }

#define _LEFTROTATE(x, n) (((x) << (n)) | ((x) >> (32-(n))))

    // add chunk to hash
    uint32_t a = digest[0],
             b = digest[1],
             c = digest[2],
             d = digest[3],
             e = digest[4];
    for(int i = 0; i < 80; ++i){
        uint32_t f, k, tmp;
        if(i >= 16){
            // extend chunk to 80 words
            w[i] = _LEFTROTATE(w[i-3] ^ w[i-8] ^ w[i-14] ^ w[i-16], 1);
        }
        if(i < 20){
            f = (b & c) | ((~b) & d);
            k = 0x5a827999;
        } else if(i < 40){
            f = b ^ c ^ d;
            k = 0x6ed9eba1;
        } else if(i < 60){
            f = (b & c) | (b & d) | (c & d);
            k = 0x8f1bbcdc;
        } else {
            f = b ^ c ^ d;
            k = 0xca62c1d6;
        }
        tmp = _LEFTROTATE(a, 5) + f + e + k + w[i];
        e = d;
        d = c;
        c = _LEFTROTATE(b, 30);
        b = a;
        a = tmp;
    }
    digest[0] += a;
    digest[1] += b;
    digest[2] += c;
    digest[3] += d;
    digest[4] += e;
    return more;

#undef _LEFTROTATE
}

void hmac_sha1(uint32_t *digest, const uint8_t *key, const uint8_t *msg, uint16_t keylen, uint16_t msglen)
{
    struct sha1_context ctx;
    uint32_t tmpdigest[5];
    uint8_t ipad[64];
    uint8_t opad[64];

    //TODO increase max keylen by hashing the key if it's longer than 64 bytes

    for(uint16_t i = 0; i < 64; ++i){
        uint8_t k = 0;
        if(i < keylen){
            k = key[i];
        }
        ipad[i] = 0x36 ^ k;
        opad[i] = 0x5c ^ k;
    }

    // hash ipad + msg into tmpdigest
    sha1_begin(tmpdigest, &ctx, ipad, 64);
    sha1_continue(tmpdigest, &ctx, false);
    ctx.data = msg;
    ctx.remaining = msglen;
    ctx.total_len += msglen;
    while(sha1_continue(tmpdigest, &ctx, true));

    // hash opad + tmpdigest into digest
    sha1_begin(digest, &ctx, opad, 64);
    sha1_continue(digest, &ctx, false);
    ctx.data = (uint8_t *)tmpdigest;
    ctx.remaining = 20;
    ctx.total_len += 20;
    sha1_continue(digest, &ctx, true);
}
