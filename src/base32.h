#ifndef BASE32_H
#define BASE32_H

#include <stdint.h>

/* Returns output length for given input. Returns 0 if invalid. */
uint16_t Base32Len(const char *in);

void Base32Decode(uint8_t *out, const char *in);

#endif
