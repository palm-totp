#define MainForm        1000
#define AboutDialog     1001
#define NewKeyDialog    1002
#define DetailsDialog   1003
#define ClockSyncDialog 1004
#define TimezoneDialog  1005

#define NoNameAlert     1000
#define BadKeyAlert     1001
#define SelectAlert     1002
#define NextAlert       1003
#define DeletePrompt    1004

#define OKBtn           1000
#define CancelBtn       1001
#define DetailsBtn      1002
#define NextCodeBtn     1003
#define UpBtn           1004
#define DownBtn         1005
#define CodeTable       1006
#define AppField        1007
#define UserField       1008
#define KeyField        1009
#define TypePopup       1010
#define TypeList        1011
#define TypeVal         1012
#define IntegrityLbl    1013
#define IntegrityVal    1014
#define CountLbl        1015
#define CountField      1016
#define CountVal        1017
#define OffsetPopup     1018
#define OffsetList      1019
#define OffsetLbl       1020
#define AutoTZChk       1021

#define MainMenu        1000
#define EditMenu        1001

#define NewItm          1000
#define DeleteItm       1001
#define TimezoneItm     1020
#define AboutItm        1021

/* tBMP */
#define LedDot		2000

/* tSTR */
#define NewKeyHelp      1000
#define DetailsHelp     1001
#define TimezoneHelp    1002
