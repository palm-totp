#include <PalmOS.h>
#include "res.h"
#include "app.h"

static Boolean _TickEnabled()
{
    UInt32 v;
    FtrGet(APP_CREATOR, FTR_TICK_ENABLE, &v);
    return v != 0;
}

static void _SetTickEnabled(Boolean enable)
{
    FtrSet(APP_CREATOR, FTR_TICK_ENABLE, enable ? 1 : 0);
}

static void MainFormInit(FormType *form){
    CodeTableInit(form);
}

static void MainFormDeInit(FormType *form){
}

static void _ClockTick(FormType *form, Boolean always_redraw)
{
    if(WinGetActiveWindow() != FrmGetWindowHandle(form)) return;
    if(!_TickEnabled()) return;
    RectangleType r;
    UInt32 oldClock;
    FtrGet(APP_CREATOR, FTR_LAST_TICK, &oldClock);
    UInt32 now = UnixNow();
    UInt32 clock = now % 30;
    r.topLeft.x = 128; r.topLeft.y = 5;
    r.extent.x = 29; r.extent.y = 4;
    if(always_redraw || now/30 != oldClock){
        WinDrawRectangleFrame(rectangleFrame, &r);
        WinEraseRectangle(&r, 0);
        CodeTableRefresh(form);
        FtrSet(APP_CREATOR, FTR_LAST_TICK, now/30);
    }
    r.extent.x = clock;
    r.topLeft.x += 29-clock;
    WinDrawRectangle(&r, 0);
}

static void _NextCode(FormType *form)
{
    TableType *tbl = FrmGetObjectPtr(form, FrmGetObjectIndex(form, CodeTable));
    DmOpenRef db;
    Int16 row = CodeTableGetSelection();
    MemHandle recH;
    struct key_record rec;
    if(row >= 0){
        FtrGet(APP_CREATOR, FTR_DB_HANDLE, (UInt32 *)&db);
        recH = DmQueryRecord(db, TblGetItemInt(tbl, row, 0));
        UnpackKeyRecord(&rec, MemHandleLock(recH));
        if(rec.header.type == OTP_TYPE_HOTP){
            CodeTableShowHOTP(tbl, row);
        } else {
            FrmAlert(NextAlert);
        }
        MemHandleUnlock(recH);
    } else {
        FrmAlert(SelectAlert);
    }
}

static void _Details(FormType *form)
{
    TableType *tbl = FrmGetObjectPtr(form, FrmGetObjectIndex(form, CodeTable));
    FormType *dialog;
    Int16 row = CodeTableGetSelection();
    if(row >= 0){
        if(DoDetailsDialog(TblGetItemInt(tbl, row, 0))){
            TblMarkRowInvalid(tbl, row);
            TblRedrawTable(tbl);
        }
    } else {
        FrmAlert(SelectAlert);
    }
}

static void _Delete(FormType *form)
{
    TableType *tbl = FrmGetObjectPtr(form, FrmGetObjectIndex(form, CodeTable));
    DmOpenRef db;
    Int16 choice, row = CodeTableGetSelection();
    if(row >= 0){
        choice = FrmAlert(DeletePrompt);
        if(choice == 0){
            FtrGet(APP_CREATOR, FTR_DB_HANDLE, (UInt32 *)&db);
            DmRemoveRecord(db, TblGetItemInt(tbl, row, 0));
            CodeTableScroll(form, 0);
        }
    } else {
        FrmAlert(SelectAlert);
    }
}

Boolean AboutDialogHandleEvent(EventType *event)
{
    /* Display secret debug values */
    FormType *form;
    struct myPrefs prefs;
    char buf[32];
    buf[0] = 0;
    RectangleType r;
    switch(event->eType){
    case keyDownEvent:
        r.topLeft.x = 0; r.topLeft.y = 120;
        r.extent.x = 156; r.extent.y = 11;
        WinEraseRectangle(&r, 0);
        switch(event->data.keyDown.chr){
        case 'P':
        case 'p':
            GetPrefs(&prefs);
            StrPrintF(buf, "Configured TZ: %d", prefs.timezone);
            break;
        case '2':
            if(GetRomVerMajor() < 2) break;
            StrPrintF(buf, "V2 system TZ: %d (%lu)", DefaultTimezoneV2(),
                    PrefGetPreference(prefMinutesWestOfGMT));
            break;
        case '4':
            if(GetRomVerMajor() < 4) break;
            StrPrintF(buf, "V4 system TZ: %d (%ld+%lu)", DefaultTimezoneV4(),
                    PrefGetPreference(prefTimeZone),
                    PrefGetPreference(prefDaylightSavingAdjustment));
            break;
        case 'U':
        case 'u':
            StrPrintF(buf, "Unix time: %lu", UnixNow());
            break;
        }
        WinDrawChars(buf, StrLen(buf), 12, 120);
        return false;
    default: return false;
    }
}

Boolean MainFormHandleEvent(EventType *event)
{
    FormType *form, *dialog;
    Int16 choice;

    switch(event->eType){
    case frmOpenEvent:
        form = FrmGetActiveForm();
        MainFormInit(form);
        FrmDrawForm(form);
        _SetTickEnabled(true);
        _ClockTick(form, true);
        return true;
    case frmUpdateEvent:
        form = FrmGetActiveForm();
        FrmDrawForm(form);
        return true;
    case winEnterEvent:
        form = FrmGetFormPtr(MainForm);
        if(form && event->data.winEnter.exitWindow != (WinHandle)form){
            //_ClockTick(form, true);
        }
        return false;
    case nilEvent:
        form = FrmGetActiveForm();
        _ClockTick(form, false);
	return true;
    case menuEvent:
        form = FrmGetActiveForm();
        switch(event->data.menu.itemID){
        case NewItm:
            if(DoNewKeyDialog()) CodeTableScroll(form, -10000);
            return true;
        case DeleteItm:
            _Delete(form);
            return true;
        case TimezoneItm:
            FrmPopupForm(TimezoneDialog);
            return true;
        case AboutItm:
            dialog = FrmInitForm(AboutDialog);
            FrmSetEventHandler(dialog, AboutDialogHandleEvent);
            FrmDoDialog(dialog);
            FrmDeleteForm(dialog);
            return true;
        }
        return false;
    case ctlSelectEvent:
        form = FrmGetActiveForm();
        switch(event->data.ctlSelect.controlID){
        case DetailsBtn:
            _Details(form);
            return true;
        case NextCodeBtn:
            _NextCode(form);
            return true;
        }
        return false;
    case ctlRepeatEvent:
        form = FrmGetActiveForm();
        switch(event->data.ctlRepeat.controlID){
        case UpBtn: CodeTableScroll(form, -5); return false;
        case DownBtn: CodeTableScroll(form, 5); return false;
        }
        return false;
    case keyDownEvent:
        form = FrmGetActiveForm();
        switch(event->data.keyDown.chr){
        case pageUpChr: CodeTableScroll(form, -5); break;
        case pageDownChr: CodeTableScroll(form, 5); break;
        }
        return true;
    case penDownEvent:
        form = FrmGetActiveForm();
        return CodeTablePenDown(form, event);
    default:
        return false;
    }
}
