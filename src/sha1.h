#ifndef SHA1_H
#define SHA1_H

#include <stdint.h>
#include <stdbool.h>

struct sha1_context {
    const uint8_t *data;
    uint16_t total_len;
    uint16_t remaining;
};

/* Simple call which combines the below begin/continue functions. */
void sha1(uint32_t *digest, const uint8_t *data, uint16_t len);

/* Set up SHA-1. digest must be 5 words long. */
void sha1_begin(uint32_t *digest, struct sha1_context *ctx, const uint8_t *data, uint16_t len);

/* Process one chunk. Set finish=false if you don't know the total length yet.
 * This allows streaming by rewriting data/total_len/remaining in ctx.
 * Otherwise, just keep running this until it returns false. */
bool sha1_continue(uint32_t *digest, struct sha1_context *ctx, bool finish);

/* Max keylen == 64 */
void hmac_sha1(uint32_t *digest, const uint8_t *key, const uint8_t *msg, uint16_t keylen, uint16_t msglen);

#endif
