#include <PalmOS.h>
#include "res.h"
#include "app.h"
#include "base32.h"

Boolean NewKeyHandleEvent(EventType *event)
{
    FormType *form;
    switch(event->eType){
    case popSelectEvent:
        form = FrmGetActiveForm();
        switch(event->data.popSelect.listID){
        case TypeList:
            if(event->data.popSelect.selection == 1){
                FrmShowObject(form, FrmGetObjectIndex(form, CountLbl));
                FrmShowObject(form, FrmGetObjectIndex(form, CountField));
            } else {
                FrmHideObject(form, FrmGetObjectIndex(form, CountLbl));
                FrmHideObject(form, FrmGetObjectIndex(form, CountField));
            }
            return false;
        }
        return false;
    default:
        return false;
    }
}

Boolean NewKeySave(FormType *form)
{
    DmOpenRef db;
    FtrGet(APP_CREATOR, FTR_DB_HANDLE, (UInt32 *)&db);
    ControlType *ctl;
    MemHandle appH, userH, keyH, recH;
    char *text;
    struct key_record rec;
    UInt8 keybuf[40];
    UInt16 recIndex;

    rec.header.code_len = 6;
    rec.header.counter = 0;

    ctl = FrmGetObjectPtr(form, FrmGetObjectIndex(form, TypeList));
    rec.header.type = LstGetSelection((ListType *)ctl);

    ctl = FrmGetObjectPtr(form, FrmGetObjectIndex(form, AppField));
    if(FldGetTextLength((FieldType *)ctl) == 0){
        FrmAlert(NoNameAlert);
        return false;
    }
    appH = FldGetTextHandle((FieldType *)ctl);
    rec.appname = MemHandleLock(appH);

    ctl = FrmGetObjectPtr(form, FrmGetObjectIndex(form, UserField));
    userH = FldGetTextHandle((FieldType *)ctl);
    if(userH){
        rec.username = MemHandleLock(userH);
    } else {
        rec.username = "";
    }

    ctl = FrmGetObjectPtr(form, FrmGetObjectIndex(form, KeyField));
    keyH = FldGetTextHandle((FieldType *)ctl);
    text = MemHandleLock(keyH);
    rec.header.key_len = Base32Len(text);
    if(rec.header.key_len == 0){
        FrmAlert(BadKeyAlert);
        MemHandleUnlock(keyH);
        return false;
    }
    Base32Decode(keybuf, text);
    rec.key = keybuf;
    MemHandleUnlock(keyH);

    recIndex = 0;
    recH = DmNewRecord(db, &recIndex, KeyRecordSize(&rec));
    PackKeyRecord(MemHandleLock(recH), &rec);
    MemHandleUnlock(recH);

    MemHandleUnlock(appH);
    if(userH) MemHandleUnlock(userH);

    DmReleaseRecord(db, recIndex, true);
    return true;
}

Boolean DoNewKeyDialog()
{
    FormType *dialog = FrmInitForm(NewKeyDialog);
    Int16 choice;
    FrmSetEventHandler(dialog, NewKeyHandleEvent);
    FldInsert(FrmGetObjectPtr(dialog, FrmGetObjectIndex(dialog, CountField)), "0", 1);
    do {
        choice = FrmDoDialog(dialog);
        if(choice == OKBtn){
            if(NewKeySave(dialog)) break;
        }
    } while(choice != CancelBtn);
    FrmDeleteForm(dialog);
    return choice == OKBtn;
}

void FldSetTextFromStr(FieldType *fld, const char *str)
{
    char buf[16];
    MemHandle textH = FldGetTextHandle(fld);
    FldSetTextHandle(fld, NULL);
    if(textH)
        MemHandleResize(textH, StrLen(str)+1);
    else
        textH = MemHandleNew(StrLen(str)+1);
    char *text = MemHandleLock(textH);
    StrCopy(text, str);
    MemHandleUnlock(textH);
    FldSetTextHandle(fld, textH);
}

void DetailsInit(FormType *form, MemHandle recH)
{
    ControlType *ctl;
    MemHandle oldH;
    char *buf;
    struct key_record rec;
    UnpackKeyRecord(&rec, MemHandleLock(recH));

    ctl = FrmGetObjectPtr(form, FrmGetObjectIndex(form, AppField));
    FldSetTextFromStr((FieldType *)ctl, rec.appname);

    ctl = FrmGetObjectPtr(form, FrmGetObjectIndex(form, UserField));
    FldSetTextFromStr((FieldType *)ctl, rec.username);

    ctl = FrmGetObjectPtr(form, FrmGetObjectIndex(form, TypeVal));
    buf = (char *)CtlGetLabel(ctl);
    StrPrintF(buf, "%s based",
            rec.header.type == OTP_TYPE_HOTP ? "Counter" : "Time");
    CtlSetLabel(ctl, buf);
    FrmShowObject(form, FrmGetObjectIndex(form, TypeVal));

    if(rec.header.type == OTP_TYPE_HOTP){
        ctl = FrmGetObjectPtr(form, FrmGetObjectIndex(form, IntegrityVal));
        buf = (char *)CtlGetLabel(ctl);
        struct otp_options options;
        options.code_length = 6;
        options.code_base = 10;
        UInt32 code = hotp(rec.key, rec.header.key_len, 0, &options);
        ZeroPad(buf, code, options.code_length);
        CtlSetLabel(ctl, buf);
        FrmShowObject(form, FrmGetObjectIndex(form, IntegrityLbl));
        FrmShowObject(form, FrmGetObjectIndex(form, IntegrityVal));

        ctl = FrmGetObjectPtr(form, FrmGetObjectIndex(form, CountVal));
        buf = (char *)CtlGetLabel(ctl);
        StrPrintF(buf, "%ld", rec.header.counter);
        CtlSetLabel(ctl, buf);
        FrmShowObject(form, FrmGetObjectIndex(form, CountLbl));
        FrmShowObject(form, FrmGetObjectIndex(form, CountVal));
    }

    MemHandleUnlock(recH);
}

Boolean DetailsSave(FormType *form, UInt16 recIdx, MemHandle *recH, Boolean *dirty)
{
    DmOpenRef db;
    FtrGet(APP_CREATOR, FTR_DB_HANDLE, (UInt32 *)&db);
    ControlType *ctl;
    struct key_record rec;
    MemHandle textH;
    UnpackKeyRecord(&rec, MemHandleLock(*recH));
    rec.key = NULL;
    MemHandleUnlock(*recH);

    ctl = FrmGetObjectPtr(form, FrmGetObjectIndex(form, AppField));
    if(FldGetTextLength((FieldType *)ctl) == 0){
        FrmAlert(NoNameAlert);
        return false;
    }
    *dirty = *dirty || FldDirty((FieldType *)ctl);
    textH = FldGetTextHandle((FieldType *)ctl);
    rec.appname = MemHandleLock(textH);

    ctl = FrmGetObjectPtr(form, FrmGetObjectIndex(form, UserField));
    *dirty = *dirty || FldDirty((FieldType *)ctl);
    textH = FldGetTextHandle((FieldType *)ctl);
    rec.username = MemHandleLock(textH);


    *recH = DmResizeRecord(db, recIdx, KeyRecordSize(&rec));
    PackKeyRecord(MemHandleLock(*recH), &rec);
    MemHandleUnlock(*recH);

    MemPtrUnlock(rec.appname);
    MemPtrUnlock(rec.username);
    return true;
}

Boolean DoDetailsDialog(UInt16 recIdx)
{
    DmOpenRef db;
    MemHandle recH;
    Int32 choice;
    Boolean dirty = false;
    FormType *dialog;
    FtrGet(APP_CREATOR, FTR_DB_HANDLE, (UInt32 *)&db);
    recH = DmGetRecord(db, recIdx);
    dialog = FrmInitForm(DetailsDialog);
    DetailsInit(dialog, recH);
    do {
        choice = FrmDoDialog(dialog);
        if(choice == OKBtn){
            if(DetailsSave(dialog, recIdx, &recH, &dirty)) break;
        }
    } while(choice != CancelBtn);
    FrmDeleteForm(dialog);
    DmReleaseRecord(db, recIdx, dirty);
    return dirty;
}

static Int16 _SelectedOffset(FormType *form)
{
    ListType *list;
    if(GetRomVerMajor() >= 4 &&
            FrmGetControlValue(form, FrmGetObjectIndex(form, AutoTZChk))){
        return DefaultTimezoneV4();
    } else {
        list = FrmGetObjectPtr(form, FrmGetObjectIndex(form, OffsetList));
        return LstGetSelection(list) - 12;
    }
}

static void _ClockTick(FormType *form, Boolean always_redraw)
{
    DateTimeType date;
    char buf[9];
    Int16 len, w;
    UInt32 clock = TimGetSeconds();
    TimSecondsToDateTime(clock, &date);
    if(date.second == 0 || always_redraw){
        FontID oldFont = FntSetFont(stdFont);
        TimeToAscii(date.hour, date.minute, tfColonAMPM, buf);
        len = StrLen(buf);
        w = FntCharsWidth(buf, len);
        WinDrawChars(buf, len, 50-w/2, 29);

        clock -= _SelectedOffset(form) * 3600;
        TimSecondsToDateTime(clock, &date);
        TimeToAscii(date.hour, date.minute, tfColonAMPM, buf);
        len = StrLen(buf);
        w = FntCharsWidth(buf, len);
        WinDrawChars(buf, len, 110-w/2, 29);

        FntSetFont(oldFont);
    }
}

static void _AutoTZUpdate(FormType *form)
{
    ControlType *ctl;
    char *label;
    if(GetRomVerMajor() >= 4 &&
            FrmGetControlValue(form, FrmGetObjectIndex(form, AutoTZChk))){
        FrmHideObject(form, FrmGetObjectIndex(form, OffsetPopup));
        ctl = FrmGetObjectPtr(form, FrmGetObjectIndex(form, OffsetList));
        label = LstGetSelectionText((ListType *)ctl, 12+DefaultTimezoneV4());
        ctl = FrmGetObjectPtr(form, FrmGetObjectIndex(form, OffsetLbl));
        CtlSetLabel(ctl, label);
        FrmShowObject(form, FrmGetObjectIndex(form, OffsetLbl));
    } else {
        FrmHideObject(form, FrmGetObjectIndex(form, OffsetLbl));
        FrmShowObject(form, FrmGetObjectIndex(form, OffsetPopup));
    }
}

static void TimezoneInit(FormType *form)
{
    ControlType *ctl;
    struct myPrefs prefs;
    Int16 sel;
    char *label;
    GetPrefs(&prefs);

    ctl = FrmGetObjectPtr(form, FrmGetObjectIndex(form, OffsetList));
    sel = prefs.timezone + 12;
    LstSetSelection((ListType *)ctl, sel);
    LstMakeItemVisible((ListType *)ctl, sel);
    label = LstGetSelectionText((ListType *)ctl, sel);
    ctl = FrmGetObjectPtr(form, FrmGetObjectIndex(form, OffsetPopup));
    CtlSetLabel(ctl, label);

    ctl = FrmGetObjectPtr(form, FrmGetObjectIndex(form, AutoTZChk));
    FrmSetControlValue(form, FrmGetObjectIndex(form, AutoTZChk), prefs.autoTZ);
    if(GetRomVerMajor() >= 4){
        FrmShowObject(form, FrmGetObjectIndex(form, AutoTZChk));
    }
    _AutoTZUpdate(form);
}

static void TimezoneSave(FormType *form)
{
    struct myPrefs prefs;
    GetPrefs(&prefs);
    prefs.timezone = _SelectedOffset(form);
    prefs.autoTZ = FrmGetControlValue(form, FrmGetObjectIndex(form, AutoTZChk));
    PrefSetAppPreferencesV10(APP_CREATOR, PREFS_VERSION, (void *)&prefs, sizeof(struct myPrefs));
}

Boolean TimezoneHandleEvent(EventType *event)
{
    FormType *form;
    switch(event->eType){
    case frmOpenEvent:
        form = FrmGetActiveForm();
        TimezoneInit(form);
        FrmDrawForm(form);
        WinDrawLine(4, 46, 152, 46);
        _ClockTick(form, true);
        return true;
    case frmUpdateEvent:
        form = FrmGetActiveForm();
        FrmDrawForm(form);
        return true;
    case nilEvent:
        form = FrmGetActiveForm();
        _ClockTick(form, false);
        return true;
    case ctlSelectEvent:
        form = FrmGetActiveForm();
        switch(event->data.ctlSelect.controlID){
        case AutoTZChk:
            _AutoTZUpdate(form);
            return false;
        case OKBtn:
            TimezoneSave(form);
            // fall through
        case CancelBtn:
            FrmReturnToForm(0);
            return true;
        }
        return false;
    case popSelectEvent:
        form = FrmGetActiveForm();
        _ClockTick(form, true);
        return false;
    default:
        return false;
    }
}
