#include "base32.h"

uint16_t Base32Len(const char *in){
    uint16_t in_len = 0;
    while(*in){
        while(*in == ' ' || *in == '=') ++in;
        if((*in >= 'a' && *in <= 'z') ||
           (*in >= 'A' && *in <= 'Z') ||
           (*in >= '0' && *in <= '7')){
            ++in; ++in_len;
        } else {
            return 0;
        }
    }
    return (in_len / 8 + (in_len % 8 != 0)) * 5;
}

void Base32Decode(uint8_t *out, const char *in){
    while(*in){
        uint8_t out_high = 0;
        uint32_t out_low = 0;
        for(int i = 0; i < 8; ++i){
            uint8_t c = 0;
            if(*in){
                while(*in == ' ' || *in == '=') ++in;
                if(*in >= 'A' && *in <= 'Z'){
                    c = *in - 65;
                } else if(*in >= 'a' && *in <= 'z'){
                    c = *in - 97;
                } else if(*in >= '2' && *in <= '7'){
                    c = *in - 24;
                } else if(*in == '0'){
                    c = 14;
                } else if(*in == '1'){
                    c = 8;
                } else {
                    return;
                }
                ++in;
            }
            out_high = (out_high << 5) | (out_low >> 27);
            out_low = (out_low << 5) | c;
        }
        *(out++) = out_high;
        *(out++) = out_low >> 24;
        *(out++) = out_low >> 16 & 0xff;
        *(out++) = out_low >> 8 & 0xff;
        *(out++) = out_low & 0xff;
    }
}
